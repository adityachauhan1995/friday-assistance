    import sys
    import re
    import webbrowser
    import smtplib
	import speech_recognition as sr
    import os
    import requests
    import subprocess
    import urllib
    import json 
    from bs4 import BeautifulSoup as soup
    import wikipedia
    import random
    from time import strftime
    def fridayResponse(audio):
        "speaks the audio passed as an argument"
        print(audio)
        for line in audio.splitlines():
            os.system("say " + audio)
    def myCommand():
        "listens for commands"
        r = sr.Recognizer()
        with sr.Microphone() as source:
            print('Say something...')
            r.pause_threshold = 1
            r.adjust_for_ambient_noise(source, duration=1)
            audio = r.listen(source)
        try:
            command = r.recognize_google(audio).lower()
            print('You said: ' + command + '\n')
        #looping to listen to a clearer command
        except sr.UnknownValueError:
            print('....')
            command = myCommand();
        return command
    def assistant(command):
        "if statements for executing commands"

        if 'shutdown' in command:
            fridayResponse('Bye bye. Have a wonderful day')
            sys.exit()
    #open website requested 
        elif 'open' in command:
            reg_ex = re.search('open (.+)', command)
            if reg_ex:
                domain = reg_ex.group(1)
                print(domain)
                url = 'https://www.' + domain
                webbrowser.open(url)
                fridayResponse('The website you have requested has been opened for you Sir.')
            else:
                pass
    #greeting the user
        elif 'hello' in command:
            day_time = int(strftime('%H'))
            if day_time < 12:
                fridayResponse('Hello. Good morning')
            else:
                fridayResponse('Hello Sir. Good evening')
        elif 'help me' in command:
            fridayResponse("""
            You can use these commands and I'll help you out:
            1. say shutdown to stop the assistant
            2. open a website.
            3. say hello to greet each other
            4. say help me to guide you
            5. Tell a joke to listen to a joke
            6. Say launch with application name to open an application
            7. Use tell me about to learn about anything
            9. Use email to send an email.. although faced an issue because of google security
            10. top stories from google news (RSS feeds)
            """)
    #randomly telling a joke using an icanhazad
        elif 'tell a joke' in command:
            res = requests.get(
                    'https://icanhazdadjoke.com/',
                    headers={"Accept":"application/json"})
            if res.status_code == requests.codes.ok:
                fridayResponse(str(res.json()['joke']))
            else:
                fridayResponse('oops!I have run out of jokes')
    
       
    
        elif 'launch' in command:
            reg_ex = re.search('launch (.*)', command)
            if reg_ex:
                appname = reg_ex.group(1)
                appname1 = appname+".app"
                subprocess.Popen(["open", "-n", "/Applications/" + appname1], stdout=subprocess.PIPE)
                fridayResponse('I have launched the desired application')
    
        elif 'tell me about' in command:
            reg_ex = re.search('tell me about (.*)', command)
            try:
                if reg_ex:
                    topic = reg_ex.group(1)
                    ny = wikipedia.page(topic)
                    fridayResponse(ny.content[:500].encode('utf-8'))
            except Exception as e:
                    print(e)
                    fridayResponse(e)
        elif 'email' in command:
              fridayResponse('Who is the recipient?')
              recipient = myCommand()
              if 'sumaira' in recipient:
                  fridayResponse('What should I say to him?')
                  content = myCommand()
                  mail = smtplib.SMTP('smtp.gmail.com', 587)
                  mail.ehlo()
                  mail.starttls()
                  mail.login('ammarpathan@gmail.com', '*********')
                  mail.sendmail('ammarpathan@gmail.com', 'skhan.tayyeba@gmail.com', content)
                  mail.close()
                  fridayResponse('Email has been sent successfuly. You can check your inbox.')
              else:
                 fridayResponse('I don\'t know what you mean!')
        elif 'news for today' in command:
              try:
                news_url="https://news.google.com/news/rss"
                Client=urllib.request.urlopen(news_url)
                xml_page=Client.read()
                Client.close()
                soup_page=soup(xml_page,"xml")
                news_list=soup_page.findAll("item")
                for news in news_list[:15]:
                    fridayResponse(news.title.text.encode('utf-8'))
              except Exception as e:
                print(e)
    fridayResponse('Hi User, I am Friday and I am your personal voice assistant, Please give a command or say "help me" and I will tell you what all I can do for you.')
    #looping to continue accepting commands
    while True:
        assistant(myCommand())  